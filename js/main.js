let todoList = [];
let finishList = [];
let addNewInfo = () => {
  let todoInfo = document.getElementById("newTask").value.trim();
  if (todoInfo != "") {
    todoList.push(todoInfo);
    document.getElementById("todo").innerHTML = renderList(todoList);
  }
};

let renderList = (list) => {
  let contentHTML = ``;
  list.forEach((item, index) => {
    let contentList = `
      <li>${item}
      <div class = "buttons">
      <button class = "remove" onclick = "removeTask(${index},todoList)"><i class="fa fa-trash-alt"></i></button>
      <button class = "complete" onclick = "addFinishList(${index})"><i class="fa fa-check-circle"></i></button>
      </div>
      
      </li>
      `;
    contentHTML += contentList;
  });

  return contentHTML;
};

let removeTask = (index, list) => {
  list.splice(index, 1);
  renderList(list);
  document.getElementById("todo").innerHTML = renderList(todoList);
};

let addFinishList = (index) => {
  let completeTask = todoList[index];
  finishList.push(completeTask);
  document.getElementById("completed").innerHTML = renderFinishList();
};

let renderFinishList = () => {
  let contentHTML = ``;
  finishList.forEach((item, index) => {
    let contentList = `
      <li>${item}
      <div class = "buttons">
      <button class = "remove" onclick = "removeCompleteTask(${index})"><i class="fa fa-trash-alt"></i></button>
      <span><i class="fa fa-check-circle"></i></span>
      </div>
      
      </li>
      `;
    contentHTML += contentList;
  });

  return contentHTML;
};

let removeCompleteTask = (index) => {
  finishList.splice(index, 1);
  renderFinishList();
  document.getElementById("completed").innerHTML = renderFinishList();
};

let sortList = () => {
  todoList.sort();
  document.getElementById("todo").innerHTML = renderList(todoList);
};

let reverseList = () => {
todoList.reverse()
document.getElementById("todo").innerHTML = renderList(todoList);
}
